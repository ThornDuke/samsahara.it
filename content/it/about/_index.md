---
title: 'About'
date: 2021-11-16
---

Ho iniziato a programmare da ragazzo, ho scritto codice usando _tanti_ linguaggi. Ora mi occupo di
programmazione _front-end_.

Il titolo del blog è un omaggio ad un film di Gabriele Salvatores.
